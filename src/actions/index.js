import * as actionTypes from './actionTypes';

export const setBeerList = beers => {return {type: actionTypes.SET_BEERS, beers: beers}};
export const setSelectedBeer = beer => {return {type: actionTypes.SET_SELECTED_BEER, beer: beer}};
export const addToBasket = beer => {return {type: actionTypes.ADD_TO_BASKET, beer: beer}};
export const removeFromBasket = beer => {return {type: actionTypes.REMOVE_FROM_BASKET, beer: beer}};
export const setFilters = filters => {return {type: actionTypes.SET_FILTERS, filters: filters}};
export const setBasketTotal = total => {return {type: actionTypes.SET_BASKET_TOTAL, total: total}}

export const setMenuIndex = menuIndex => {return {type: actionTypes.SET_MENU_INDEX, menuIndex: menuIndex}};
export const setViewIndex = viewIndex => {return {type: actionTypes.SET_VIEW_INDEX, viewIndex: viewIndex}};
export const setScrollingDown = isScrollingDown => {return {type: actionTypes.SET_SCROLLING_DOWN, isScrollingDown: isScrollingDown}};
export const setShowBlurModal = showBlurModal => {return {type: actionTypes.SET_SHOW_BLUR_MODAL, showBlurModal: showBlurModal}}
export const setOpenCheckout = openCheckout => {return {type: actionTypes.SET_OPEN_CHECKOUT, openCheckout: openCheckout}}

export const decreaseQuantity = beer => {return {type: actionTypes.DECREASE_QUANTITY, beer: beer}}
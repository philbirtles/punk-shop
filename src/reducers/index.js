import * as actionTypes from '../actions/actionTypes';
import { combineReducers } from 'redux';

export const beersList = (beers=[], action) => {
	switch(action.type){
		case actionTypes.SET_BEERS: {
			return action.beers;
		}
		default: {
			return beers;
		};
	}
}

export const selectedBeer = (beer = null, action) => {
	switch(action.type){
		case actionTypes.SET_SELECTED_BEER: {
			return action.beer;
		}
		default: {
			return beer;
		}
	}
}

export const basketContents = (contents = [], action) => {
	switch(action.type){
		case actionTypes.ADD_TO_BASKET: {
			let itemExists = contents.find(beer => {
				return beer.id === action.beer.id;
			});
			if(itemExists){
				let updatedBasket = [...contents];
				return updatedBasket.map((beer) => {
					if(beer.id == action.beer.id){
						return {...beer, quantity: beer.quantity + 1};
					}else{
						return{...beer}
					}
				})
			}else{
				return [
					...contents, 
					{...action.beer, quantity: 1}
				]				
			}
		}
		case actionTypes.REMOVE_FROM_BASKET: {
			let updatedContents = [...contents];
			updatedContents.splice(updatedContents.findIndex((beer) => {
				return beer.id === action.beer.id;
			}), 1);
			return updatedContents;
		}
		case actionTypes.DECREASE_QUANTITY: {
			let itemExists = contents.find(beer => {
				return beer.id === action.beer.id;
			});
			if(itemExists){
				let updatedBasket = [...contents];
				return updatedBasket.map((beer) => {
					if(beer.id == action.beer.id){
						return {...beer, quantity: beer.quantity - 1};
					}else{
						return{...beer}
					}
				})
			}else{
				return [
					contents, 
				]				
			}
		}
		default: {
			return contents;
		}
	}
}

export const basketTotal = (total = 0, action) => {
	switch(action.type){
		case actionTypes.SET_BASKET_TOTAL: {
			return action.total
		}
		default: {
			return total;
		}
	}
}

export const menuIndex = (menuIndex = 0, action) => {
	switch(action.type){
		case actionTypes.SET_MENU_INDEX: {
			return action.menuIndex;
		}
		default: {
			return menuIndex;
		}
	}
}

export const viewIndex = (index = 0, action) => {
	switch(action.type){
		case actionTypes.SET_VIEW_INDEX: {
			return action.viewIndex;
		}
		default: {
			return index;
		}
	}
}

export const scrollingDown = (isScrollingDown=false, action) => {
	switch(action.type){
		case actionTypes.SET_SCROLLING_DOWN: {
			return action.isScrollingDown;
		}
		default: {
			return isScrollingDown
		}
	}
}

export const showBlurModal = (showBlurModal=false, action) => {
	switch(action.type){
		case actionTypes.SET_SHOW_BLUR_MODAL: {
			return action.showBlurModal
		}
		default: {
			return showBlurModal			
		}
	}
}

export const openCheckout = (openCheckout = false, action) => {
	switch(action.type){
		case actionTypes.SET_OPEN_CHECKOUT: {
			return action.openCheckout
		}
		default: {
			return openCheckout
		}
	}
}

export default combineReducers({
	beersList: beersList,
	selectedBeer: selectedBeer,
	basketContents: basketContents,
	basketTotal: basketTotal,
	menuIndex: menuIndex,
	viewIndex: viewIndex,
	scrollingDown: scrollingDown,
	showBlurModal: showBlurModal,
	openCheckout: openCheckout,
})

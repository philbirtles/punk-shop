import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react';

import './sass/core.scss';
import reducers from './reducers';
import ShopWrapper from './components/shop-wrapper';

const persistConfig = {
	key: 'root21',
	storage,
	blacklist: ['scrollingDown']
}

const persistedReducer = persistReducer(persistConfig, reducers);

const store = createStore(persistedReducer)
let persistor = persistStore(store)

function App() {
return (
	<div className="App">
		<Provider store={store}>
			<PersistGate loading={null} persistor={persistor}>
				<ShopWrapper />
			</PersistGate>
		</Provider>
	</div>);
}

export default App;

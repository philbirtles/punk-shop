import Axios from 'axios';

export function fetchData(
		url="",
		params={},
		dataHandler=()=>{},
		errorHandler=(error)=>{console.log(error)},
		complete=()=>{}
		)
	{
		Axios.get(url, {params: params})
			.then((response) => dataHandler(response.data))
			.catch((error) => errorHandler(error))
			.then(() => complete())
	}


import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setMenuIndex, setViewIndex } from '../actions';
import MenuItem from './menu-item';
import MenuItemHighlighter from './menu-item-highlighter';

class Menu extends Component {
	constructor(props){
		super(props);
		this.setMenuIndex = this.setMenuIndex.bind(this);
		this.state = {
			menuIndex: props.menuIndex
		}
	}
	setMenuIndex(menuIndex){
		this.setState({menuIndex: menuIndex});
		this.props.setMenuIndex(menuIndex);
	}
	render(){
		return (
			<ul className="menu">
				<MenuItemHighlighter position={this.props.menuIndex} />
				<MenuItem onClick={() => {this.setMenuIndex(0)}} icon="/images/drinks.svg" />
				<MenuItem onClick={() => {this.setMenuIndex(1)}} icon="/images/food.svg" />
				<MenuItem onClick={() => {this.setMenuIndex(2)}} icon="/images/discount.svg" />
				<MenuItem onClick={() => {this.setMenuIndex(3)}} icon="/images/search.svg" />
			</ul>
		)
	}

}

const mapStateToProps = (state) => {
	return {
		position: state.position
	}
}

export default connect(mapStateToProps, {setMenuIndex, setViewIndex})(Menu);
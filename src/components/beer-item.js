import React, { useState } from 'react';
import { connect } from 'react-redux';

import { addToBasket, setBasketTotal, setSelectedBeer, setShowBlurModal } from '../actions';

const BeerItem = (props) => {

	const [isFavourite, setIsFavourite] = useState(false);

	const handleAddToBasket = (beer) => {
		props.addToBasket(beer);
		props.setBasketTotal(props.basketTotal + beer.abv);
	}
	const handleSelectBeer = (beer) => {
		props.setSelectedBeer(beer);
		props.setShowBlurModal(true);
	}
	return(
		<li className="beer-item" onClick={() => handleSelectBeer(props.beer)}>
			<button className="beer-item__quick-add" onClick={(e) => {e.stopPropagation(); handleAddToBasket(props.beer)}}>
				<img src="/images/add-basket3.svg"></img>
			</button>
			<div className="beer-item__abv">{props.beer.abv + "%"}</div>
			<img src={props.beer.image_url || '/images/no-image-available.png'}/>
			<div className="beer-item__copy">
				<div className="beer-item__favourite" onClick={(e) => {e.stopPropagation(); setIsFavourite(!isFavourite)}} ><img src={isFavourite ? "/images/star-on.svg" : "/images/star.svg"}/></div>
				<div className="beer-item__name">{props.beer.name}</div>
				<div className="beer-item__price">{"£" + props.beer.abv.toFixed(2)}</div>
			</div>
		</li>
	)
}


const mapStateToProps = (state) => {
	return {
		basketContents: state.basketContents,
		basketTotal: state.basketTotal,
		selectedBeer: state.selectedBeer,
		showBlurModal: state.showBlurModal
	}
}


export default connect(mapStateToProps, {addToBasket, setBasketTotal, setSelectedBeer, setShowBlurModal})(BeerItem);
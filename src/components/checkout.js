import React, { Component } from 'react';
import { connect } from 'react-redux';

import CheckoutBasketList from './checkout-basket-list';
import CheckoutPurchase from './checkout-purchase';
import { setShowBlurModal, setOpenCheckout } from '../actions';

class Checkout extends Component {
	constructor(props){
		super(props);
		this.toggleOpenShoppingCart = this.toggleOpenShoppingCart.bind(this);
	}
	toggleOpenShoppingCart(){
		this.props.setShowBlurModal(!this.props.openCheckout);
		this.props.setOpenCheckout(!this.props.openCheckout);
	}

	getTotalQuantity(){
		const items = this.props.basketContents;
		let quantity = 0;
		for(let i in items){
			quantity += items[i].quantity;
		}
		return <span>{quantity}</span>;
	}
	render(){
		return(
			<div className={this.props.openCheckout ? "checkout open" : "checkout" }>
				<div className={this.props.basketContents.length > 0 ? "checkout__item-count" : "checkout__item-count hidden"}>{this.getTotalQuantity()}</div>
				<div onTouchStart={this.toggleOpenShoppingCart} className="checkout__handle" onMouseDown={this.toggleOpenShoppingCart}>
					<div className="checkout__handle_bar"/>
					<div className="checkout__title"><span><img src="/images/shopping-basket.svg"></img>Shopping Cart</span></div>
				</div>
				<CheckoutBasketList basketContents={this.props.basketContents} />
				<CheckoutPurchase toggleShoppingCart={this.toggleOpenShoppingCart} />
				
			</div>
		)

	}
}


const mapStateToProps = (state) => {
	return {
		openCheckout: state.openCheckout,
		showBlurModal: state.showBlurModal,
		basketContents: state.basketContents,
		basketTotal: state.basketTotal

	}
}

export default connect(mapStateToProps, {setOpenCheckout, setShowBlurModal})(Checkout);
import React from 'react';
import { connect } from 'react-redux';

const CheckoutPurchase = (props) => {
	const formatBasketTotal = (total) =>{
		return "£" + (Math.abs(parseFloat(total)).toFixed(2));
	}
	return (
		<div className="checkout__final-form">
			<div className="checkout__totals">
				<span className="checkout__total-label">Total</span>
				<span className="checkout__total">{formatBasketTotal(props.basketTotal)}</span>
				<div className="clear"></div>
			</div>
			<button className="checkout__confirm">Confirm Payment</button>
			<button onClick={props.toggleShoppingCart} className="checkout__cancel">Cancel</button>
		</div>
	)
}

const mapStateToProps = (state) => {
	return {
		basketTotal: state.basketTotal

	}
}

export default connect(mapStateToProps)(CheckoutPurchase);
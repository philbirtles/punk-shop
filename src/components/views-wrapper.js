import React from 'react';
import Pager from 'react-page-controller';
import { connect } from 'react-redux';

import { setMenuIndex, setViewIndex } from '../actions';

import ShoppingList from './shopping-list';
import SubMenu from './sub-menu';

const ViewsWrapper = (props) => {
	const onMoveShouldSet = (state, e, suggested) => {
		if(suggested){
			if(e.target.attributes.override || (state.delta[0] > 0 && props.viewIndex === 0 || state.delta[0] < 0 && props.viewIndex === 2) ){
				return true;
			}
		}
		return false;
	}
	const onTerminationRequest = (state, e) => {
		if(e.target.attributes.override || (state.delta[0] > 0 && props.viewIndex === 0 || state.delta[0] < 0 && props.viewIndex === 2)) {
			return false;
		}
		return true;
	}
	return (
		<div className="views-wrapper">
			<Pager
				onMoveShouldSet={onMoveShouldSet}
				onTerminationRequest={onTerminationRequest}
				value={props.menuIndex}
				onRequestChange={(i) => props.setMenuIndex(i)}
			>
				<div>
					<SubMenu options={[{name: "All", index: 0}, {name: "Pizza", index: 1}, {name: "Steak", index: 2}]} />
					<Pager value={props.viewIndex} onRequestChange={i => props.setViewIndex(i)}>
						<ShoppingList />
						<ShoppingList food="pizza" />
						<ShoppingList food="steak" />
					</Pager>	
				</div>
				<div>
					<SubMenu options={[{name: "Food", index: 0}]} />
					<div className="empty-view food" override="nochild">
					</div>
				</div>
				<div>
					<SubMenu options={[{name: "Discounts", index: 0}]} />
					<div className="empty-view discounts" override="nochild">
					</div>
				</div>
				<div>
					<SubMenu options={[{name: "Search", index: 0}]} />
					<div className="empty-view search" override="nochild">
					</div>
				</div>

				
			</Pager>
		</div>
	)
}

const mapStateToProps = (state) => {
	return {
		menuIndex: state.menuIndex,
		viewIndex: state.viewIndex,
	}
}

export default connect(mapStateToProps, {setViewIndex, setMenuIndex})(ViewsWrapper);
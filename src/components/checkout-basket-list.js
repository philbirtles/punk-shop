import React from 'react';
import CheckoutBasketItem from './checkout-basket-item';

const CheckoutBasketList = (props) => {
	const renderEmptyListHolder = (basket) => {
		if(basket.length < 1){
			return(
				<li className="checkout__basket-empty">
					<img src="/images/empty-checkout.svg"></img>
					<span>Your basket is empty.</span>
				</li>
			)
		}
	}
	return (
		<ul className="checkout__basket-list">
		{renderEmptyListHolder(props.basketContents)}
		{props.basketContents.map((beer, index) => {
			return(
				<CheckoutBasketItem key={index} beer={beer} />
			)
		})}
		</ul>
	)
}

export default CheckoutBasketList;
import React from 'react';
import { addToBasket, decreaseQuantity, removeFromBasket, setBasketTotal } from '../actions';
import { connect } from 'react-redux';

const CheckoutBasketItem = (props) => {
	const handleDeleteFromBasket = (beer) => {
		props.removeFromBasket(beer);
		props.setBasketTotal(props.basketTotal - (beer.abv * beer.quantity))
	}
	const handleDecreaseQuantity = (beer) => {
		if(beer.quantity - 1 < 1){
			props.removeFromBasket(beer);
		}else{
			props.decreaseQuantity(beer);
		}
		props.setBasketTotal(props.basketTotal - (beer.abv));
	}
	const handleIncreaseQuantity = (beer) => {
		props.addToBasket(beer);
		props.setBasketTotal(props.basketTotal + (beer.abv));
	}
	return (
		<li className="checkout__basket-item">
			<div className="checkout__basket-item_img-wrapper">
				<img src={props.beer.image_url || '/images/no-image-available.png'}></img>
				<div className="checkout__basket-item_img-price">{"£" + props.beer.abv.toFixed(2)}</div>
			</div>
			<div className="checkout__basket-item_copy-wrapper">
				<h4>{props.beer.name}</h4>	
				<p>{props.beer.tagline}</p>
			</div>
			<div className="checkout__basket-item_modifiers">
				<button onClick={() => handleDecreaseQuantity(props.beer)} className="button">-</button>
				<span>{props.beer.quantity}</span>
				<button onClick={() => handleIncreaseQuantity(props.beer)} className="button primary">+</button>
				<button onClick={() => handleDeleteFromBasket(props.beer)} className="delete"><img src="/images/delete.svg"></img></button>
			</div>
			<div className="clear"></div>
		</li>
	)
}

const mapStateToProps = (state) => {
	return {
		basketContents: state.basketContents,
		basketTotal: state.basketTotal
	}
}

export default connect(mapStateToProps, { addToBasket, decreaseQuantity, setBasketTotal, removeFromBasket })(CheckoutBasketItem);
import React from 'react';

const MenuItemHighlighter = (props) => {
	return (
		<li className="menu__item-highlighter" style={{left: (props.position * 25) +"%"}}>
			<div className="menu__item-highlighter_edge front"></div>
			<div className="menu__item-highlighter_cover front"></div>
			<div className="menu__item-highlighter_edge"></div>
			<div className="menu__item-highlighter_cover"></div>
		</li>
	)
}

export default MenuItemHighlighter;
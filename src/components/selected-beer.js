import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setShowBlurModal, setSelectedBeer, addToBasket, setBasketTotal } from '../actions';

class SelectedBeer extends Component {
	constructor(props){
		super(props);
		this.state = {
			copyTabId: 0,

		}
		this.handleClose = this.handleClose.bind(this);
		this.handleAddToBasket = this.handleAddToBasket.bind(this);
	}
	handleAddToBasket(beer){
		this.props.addToBasket(beer);
		this.props.setBasketTotal(this.props.basketTotal + beer.abv);
	}
	handleClose(){
		this.props.setSelectedBeer(null);
		this.props.setShowBlurModal(false);
	}
	render(){
		return(
			<div className="selected-beer">
				<div onClick={this.handleClose} className="selected-beer__close"><img src="images/cancel.svg"></img></div>
				<div className="selected-beer__name">{this.props.selectedBeer.name}</div>
				<div className="selected-beer__tagline">{'"' + this.props.selectedBeer.tagline + '"'}</div>
				<img src={this.props.selectedBeer.image_url ||  '/images/no-image-available.png'} />
				<div className="selected-beer__tabs">
					<div onClick={() => this.setState({copyTabId: 0})} className={ this.state.copyTabId === 0 ? "selected-beer__tab active" : "selected-beer__tab" }>Description</div>
					<div onClick={() => this.setState({copyTabId: 1})} className={ this.state.copyTabId === 1 ? "selected-beer__tab active" : "selected-beer__tab" }>Pairing</div>
				</div>
				<div className="selected-beer__copy-wrapper">
					<p className="selected-beer__copy">{this.state.copyTabId === 0 ? this.props.selectedBeer.description : this.props.selectedBeer.food_pairing.map((food, index) => <span style={{display: "inline-block",width: "100%"}} key={index}>{food}</span>) }</p>	
				</div>
				<button className="selected-beer__add" onClick={() => {this.handleAddToBasket(this.props.selectedBeer)}}>Add to Basket</button>
			</div>
		)	
	}
} 

const mapStateToProps = (state) => {
	return {
		showBlurModal: state.showBlurModal,
		selectedBeer: state.selectedBeer,
		basketContents: state.basketContents,
		basketTotal: state.basketTotal
	}
}

export default connect(mapStateToProps, {setShowBlurModal, setSelectedBeer, addToBasket, setBasketTotal})(SelectedBeer);
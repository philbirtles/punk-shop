import React from 'react';
import { connect } from 'react-redux';

import { setMenuIndex, setViewIndex } from '../actions';

import Title from './title';
import Menu from './menu';
import BlurModal from './blur-modal';
import SelectedBeer from './selected-beer';
import Checkout from './checkout';
import LandscapeCover from './landscape-cover';
import ViewsWrapper from './views-wrapper';

function ShopWrapper(props) {
	const renderSelectedBeer = () => {
		if(props.selectedBeer){
			return ( <SelectedBeer />)
		}
	}
	return(
		<div className="shop-wrapper">
			<LandscapeCover />
			<header>
				<Title title="Brewdog Shop" />
				<Menu menuIndex={props.menuIndex} />
			</header>
			<BlurModal />
			{renderSelectedBeer()}
			<Checkout />
			<ViewsWrapper />
		</div>
	)
}

const mapStateToProps = (state) => {
	return {
		menuIndex: state.menuIndex,
		viewIndex: state.viewIndex,
		selectedBeer: state.selectedBeer
	}
}

export default connect(mapStateToProps, {setViewIndex, setMenuIndex})(ShopWrapper);
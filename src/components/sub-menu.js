import React from 'react';
import { connect } from 'react-redux';


import { setViewIndex, setScrollingDown } from '../actions';

const SubMenu = (props) => {
	return (
		<div className={props.scrollingDown ? "sub-menu__holder hidden" : "sub-menu__holder"}>
			<ul className="sub-menu" style={{left: "-" + (((props.viewIndex - 1) * (1/3)) * 100) + "%" }}>
				{props.options.map((option) => {
					return (<li key={option.index} className={option.index === props.viewIndex ? "active" : ""} onClick={() => props.setViewIndex(option.index)}>{option.name}</li>);
				})}
			</ul>
		</div>
	)
}


const mapStateToProps = (state) => {
	return {
		viewIndex: state.viewIndex,
		scrollingDown: state.scrollingDown

	}
}

export default connect(mapStateToProps, {setViewIndex, setScrollingDown})(SubMenu);
import React from 'react';

const LandscapeCover = () => {
	return(
		<div className="landscape-cover">
			<span>Rotate device to use App.</span>
		</div>
	)
} 

export default LandscapeCover;
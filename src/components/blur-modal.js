import React from 'react';
import { connect } from 'react-redux';

import { setShowBlurModal, setSelectedBeer, setOpenCheckout } from '../actions';

const BlurModal = (props) => {
	const handleClick = () => {
		props.setShowBlurModal(false);
		props.setSelectedBeer(null);
		props.setOpenCheckout(false);
	}
	return(
		<div className={props.showBlurModal ? "blur-modal" : "blur-modal hidden"} onClick={handleClick}></div>
	)
}
const mapStateToProps = (state) => {
	return {
		showBlurModal: state.showBlurModal
	}
}

export default connect(mapStateToProps, {setShowBlurModal, setSelectedBeer, setOpenCheckout})(BlurModal);
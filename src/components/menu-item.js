import React from 'react';

const MenuItem = (props) => {
	return (
		<li className="menu__item" onClick={props.onClick}>
			<div>
				<img src={props.icon}></img>	
			</div>
		</li>
	)
}

export default MenuItem;
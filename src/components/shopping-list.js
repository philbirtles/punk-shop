import React, { Component } from 'react';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';

import { fetchData } from './utils';
import { setBeerList, setScrollingDown } from '../actions';

import BeerItem from './beer-item';

class ShoppingList extends Component {
	constructor(props){
		super(props);
		this.fetchData = fetchData.bind(this);
		this.state = {
			beerList: [],
			moreToFetch: true,
			scrollPosition: 0,
			scrollingDown: this.props.scrollingDown
		}
		this.addBeersToState = this.addBeersToState.bind(this);
		this.handleScroll = this.handleScroll.bind(this);
	}
	addBeersToState(beers){
		if(beers.length < 1){
			this.setState({moreToFetch: false})
		}
		else{
			this.setState({beerList: [...this.state.beerList, ...beers]})	
		}
		
	}
	handleScroll(y){
		if(y < this.state.scrollPosition || y < 100){
			this.props.setScrollingDown(false);
		}else{
			this.props.setScrollingDown(true);
		}
		this.setState({scrollPosition: y})
	}
	componentDidMount(){
		this.fetchData("https://api.punkapi.com/v2/beers", {...this.props.filters, food: this.props.food}, this.addBeersToState)
	}
	render(){
		const beers = [];
		this.state.beerList.map((beer, i) => {
			beers.push(<BeerItem key={i} beer={beer} />)
		})
		return(
				<div className="beer-list__scroller" onScroll={(a) => this.handleScroll(a.currentTarget.scrollTop)}>
					<InfiniteScroll
						useWindow={false}
						element={'ul'}
						className={'beer-list'}
						pageStart={0}
						loadMore={(page) => {
							this.fetchData("https://api.punkapi.com/v2/beers", {...this.props.filters, page: page, food: this.props.food}, this.addBeersToState);
						}}
						hasMore={this.state.moreToFetch}
						loader={<div key="beer-list__loader">Loading...</div>}
					>
						{beers}
					</InfiniteScroll>
				</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		beerList: state.beerList,
		scrollingDown: state.scrollingDown
	}
}

export default connect(mapStateToProps, {setBeerList, setScrollingDown})(ShoppingList);